#Perbedaan Lazy dan Eager Instantiation

Lazy Instantiation akan membuat instance dari object hanya ketika method getInstance() dipanggil 
untuk pertama kali. Cara implementasinya yaitu pada static variable object dibuat null, kemudian 
baru saat getInstance() dipanggil instance akan dibuat seperti biasa dengan new.<br>

Eager Instantiation akan langsung membuat instance ketika class di inisialisasi, tanpa perlu memanggil
method getInstance(). Cara implementasinya yaitu pada static variable object langsung dibuat instance
dari class tersebut dengan new. Kemudian getInstance() akan langsung mengembalikan instance dari class.

#Keuntungan dan Kekurangan Lazy Instantiation
Keuntungan: <br>
- Mengurangi waktu loading dari aplikasi karena instance tidak langsung dibuat.<br>
- Menghemat system resource karena instance akan dibuat hanya ketika dibutuhkan.<br>

Kekurangan: <br>
- Karena class/instance tidak langsung dibuat, tidak dapat dilihat apabila terdapat error pada class.<br>
- Ketika dua thread secara bersamaan mengakses getInstance(), maka kedua thread akan membuat instance
class tersebut (race condition).<br>
- Memerlukan loading ketika pertama kali akan digunakan.<br>

#Keuntungan dan Kekurangan Eager Instantiation
Keuntungan: <br>
- Tidak memerlukan loading ketika object akan digunakan.<br>

Kekurangan: <br>
- Apabila objectnya kompleks maka bisa saja memakan resource yang besar, padahal mungkin tidak terpakai.<br>
- Waktu loading pertama kali lebih lama daripada lazy initialization, terlebih ketika aplikasi menggunakan banyak singleton.