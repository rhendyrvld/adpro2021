package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import com.sun.org.apache.xpath.internal.operations.Or;

import java.lang.Thread;

public class OrderDrink {

    private String drink;

    private static OrderDrink orderDrink = null;

    private OrderDrink() {
        drink = "None";
    }

    //Todo : Complete Me with lazy instantiation approach
    public static OrderDrink getInstance() {
        if (orderDrink == null){
            orderDrink = new OrderDrink();
        }
        return orderDrink;
    }

    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    @Override
    public String toString() {
        return drink;
    }
}
