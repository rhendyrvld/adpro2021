package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {
    private Class<?> orderServiceClass;

    @Mock
    private OrderFood orderFood;

    @Mock
    private OrderDrink orderDrink;

    @InjectMocks
    private OrderServiceImpl orderService;

    @BeforeEach
    public void setup() throws Exception {
        orderServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
    }

    @Test
    public void testOrderADrink(){
        orderService.orderADrink("Kopi");
        verify(orderDrink, times(1)).setDrink("Kopi");
    }

    @Test
    public void testGetDrink(){
        assertEquals(orderService.getDrink(), orderDrink);
    }

    @Test
    public void testOrderAFood(){
        orderService.orderAFood("Ayam Goyeng");
        verify(orderFood, times(1)).setFood("Ayam Goyeng");
    }

    @Test
    public void testGetFood(){
        assertEquals(orderService.getFood(), orderFood);
    }
}
