package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class OrderDrinkTest {
    private OrderDrink orderDrink;

    @Test
    public void testOrderDrinkIsSingleton(){
        OrderDrink orderDrink1 = orderDrink.getInstance();
        OrderDrink orderDrink2 = orderDrink.getInstance();

        assertEquals(orderDrink1, orderDrink2);
    }
}
