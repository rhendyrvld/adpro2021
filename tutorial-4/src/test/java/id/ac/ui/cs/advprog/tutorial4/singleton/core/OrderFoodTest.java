package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OrderFoodTest {
    private OrderFood orderFood;

    @Test
    public void testOrderDrinkIsSingleton(){
        OrderFood orderFood1 = orderFood.getInstance();
        OrderFood orderFood2 = orderFood.getInstance();
        assertEquals(orderFood1, orderFood2);
    }
}
