package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarCipherTransformation implements Transformation{
    private int offset;

    public CaesarCipherTransformation(){
        this.offset = 5;
    }

    public CaesarCipherTransformation(int offset){
        this.offset = offset;
    }

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    public Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int shift = encode ? offset : -offset;
        int codexLength = codex.getCharSize();
        int n = text.length();
        StringBuilder result = new StringBuilder();
        for (char character : text.toCharArray()){
            int newIdx = (codex.getIndex(character) + shift) % codexLength;
            newIdx = newIdx < 0 ? newIdx + codexLength : newIdx;
            result.append(codex.getChar(newIdx));
        }
        return new Spell(result.toString(), codex);
    }
}
