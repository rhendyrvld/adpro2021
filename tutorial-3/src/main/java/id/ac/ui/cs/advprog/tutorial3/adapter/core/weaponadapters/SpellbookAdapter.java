package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean isChargedLast;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.isChargedLast = false;
    }

    @Override
    public String normalAttack() {
        this.isChargedLast = false;
        return this.spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (!this.isChargedLast) {
            this.isChargedLast = true;
        }
        else {
            this.isChargedLast = false;
        }
        return this.isChargedLast ? this.spellbook.largeSpell() : "Failed to use charged attack again";
    }

    @Override
    public String getName() {
        return this.spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return this.spellbook.getHolderName();
    }

    public boolean getIsChargedLast(){
        return this.isChargedLast;
    }

    public void setChargedLast(boolean isChargedLast){
        this.isChargedLast = isChargedLast;
    }

}
