package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.IonicBow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;


    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        for (Bow bow : bowRepository.findAll()){
            if(weaponRepository.findByAlias(bow.getName()) == null){
                weaponRepository.save(new BowAdapter(bow));
            }
        }
        for (Spellbook spellbook : spellbookRepository.findAll()){
            if(weaponRepository.findByAlias(spellbook.getName()) == null){
                weaponRepository.save(new SpellbookAdapter(spellbook));
            }
        }
        List<Weapon> weapons = weaponRepository.findAll();

        return weapons;
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        if (weapon == null){
            Bow bow = bowRepository.findByAlias(weaponName);
            if (bow != null){
                weapon = new BowAdapter(bow);
            }
        }
        if (weapon == null){
            Spellbook spellbook = spellbookRepository.findByAlias(weaponName);
            if (spellbook != null){
                weapon = new SpellbookAdapter(spellbook);
            }
        }
        weaponRepository.save(weapon);

        logRepository.addLog(String.format("%s attacked with %s (%s attack): %s ",
                weapon.getHolderName(),
                weapon.getName(),
                attackType == 0 ? "normal" : "charged",
                attackType == 0 ? weapon.normalAttack() : weapon.chargedAttack()
                )
        );
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
